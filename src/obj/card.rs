// SPDX-License-Identifier: GPL-3.0-or-later
use gtk::glib;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct Card {}

    #[glib::object_subclass]
    impl ObjectSubclass for Card {
        const NAME: &'static str = "CardObj";
        type Type = super::Card;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for Card {}
}

glib::wrapper! {
    pub struct Card(ObjectSubclass<imp::Card>);
}

impl Default for Card {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl Card {}
