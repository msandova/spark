// SPDX-License-Identifier: GPL-3.0-or-later
use crate::model::Model;

use once_cell::sync::Lazy;
use sqlx::sqlite::SqlitePoolOptions;
use std::path::PathBuf;

use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk_macros::spawn;

#[derive(sqlx::FromRow, Debug)]
struct Col {
    pub models: String,
    pub decks: String,
    pub dconf: String,
}

mod imp {
    use super::*;

    use std::cell::{Cell, RefCell};

    #[derive(Debug, Default)]
    pub struct Deck {
        pub name: RefCell<Option<String>>,
        pub path: RefCell<Option<String>>,
        pub desc: RefCell<Option<String>>,
        pub cache_path: RefCell<Option<String>>,
        pub per_day: Cell<i32>,
        pub models: RefCell<Vec<Model>>,

        pub pool: RefCell<Option<sqlx::SqlitePool>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Deck {
        const NAME: &'static str = "Deck";
        type Type = super::Deck;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for Deck {
        fn dispose(&self, obj: &Self::Type) {
            if let Some(pool) = obj.pool() {
                spawn!(async move {
                    pool.close().await;
                });
            }
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpec::new_string(
                        "name",
                        "name",
                        "name",
                        None,
                        glib::ParamFlags::READWRITE,
                    ),
                    glib::ParamSpec::new_string(
                        "path",
                        "Path",
                        "Path",
                        None,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                    ),
                    glib::ParamSpec::new_string(
                        "desc",
                        "desc",
                        "desc",
                        None,
                        glib::ParamFlags::READWRITE,
                    ),
                    glib::ParamSpec::new_int(
                        "per-day",
                        "Per Day",
                        "Per Day",
                        0,
                        std::i32::MAX,
                        20,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::EXPLICIT_NOTIFY,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "name" => self.name.borrow().to_value(),
                "path" => self.path.borrow().to_value(),
                "desc" => self.desc.borrow().to_value(),
                "per-day" => obj.new_per_day().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "name" => {
                    self.name.replace(value.get().unwrap());
                }
                "path" => {
                    self.path.replace(value.get().unwrap());
                }
                "desc" => {
                    self.desc.replace(value.get().unwrap());
                }
                "per-day" => obj.set_new_per_day(value.get().unwrap()),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct Deck(ObjectSubclass<imp::Deck>);
}

impl Deck {
    pub fn from_path(path: &str) -> Self {
        let deck: Self = glib::Object::new(&[("path", &path.to_string())]).unwrap();

        gtk_macros::spawn!(glib::clone!(@weak deck => async move {
            deck.set_properties().await.unwrap();
        }));

        deck
    }

    pub fn path(&self) -> PathBuf {
        let path: String = self.property("path").unwrap().get().unwrap();
        PathBuf::from(path)
    }

    pub fn set_path(&self) -> PathBuf {
        let path: String = self.property("path").unwrap().get().unwrap();
        PathBuf::from(path)
    }

    pub fn name(&self) -> String {
        self.property("name").unwrap().get().unwrap()
    }

    pub fn set_name(&self, name: &str) {
        self.set_property("name", name).unwrap();
    }

    pub fn new_per_day(&self) -> i32 {
        let self_ = imp::Deck::from_instance(self);

        self_.per_day.get()
    }

    pub fn set_new_per_day(&self, per_day: i32) {
        let self_ = imp::Deck::from_instance(self);

        self_.per_day.set(per_day);
        self.notify("per-day");
    }

    pub fn cache_path(&self) -> PathBuf {
        let path = self.path();
        let deck_name = path.file_stem().unwrap();

        glib::user_cache_dir()
            .join("Spark")
            .join("decks")
            .join(deck_name)
    }

    pub fn cache_deck(&self) -> anyhow::Result<()> {
        let path = self.path();
        let file_path = std::path::Path::new(&path);
        let file = std::fs::File::open(file_path)?;
        let decks_path = self.cache_path();

        let mut archive = zip::ZipArchive::new(file)?;
        archive.extract(decks_path)?;

        Ok(())
    }

    async fn init_pool(&self, path: PathBuf) -> anyhow::Result<()> {
        let self_ = imp::Deck::from_instance(self);

        let pool = SqlitePoolOptions::new()
            .max_connections(5)
            .connect(path.to_str().unwrap())
            .await?;

        self_.pool.replace(Some(pool));
        Ok(())
    }

    fn pool(&self) -> Option<sqlx::SqlitePool> {
        let self_ = imp::Deck::from_instance(self);

        self_.pool.borrow().clone()
    }

    async fn set_properties(&self) -> anyhow::Result<()> {
        let self_ = imp::Deck::from_instance(self);

        self.cache_deck()?;

        let path = self.cache_path().join("collection.anki2");
        self.init_pool(path).await?;
        let pool = self.pool().unwrap();

        let col = sqlx::query_as::<_, Col>("SELECT models, decks, dconf FROM col")
            .fetch_one(&pool)
            .await?;
        let decks = json::parse(&col.decks).unwrap();
        let models = json::parse(&col.models).unwrap();

        for (key, value) in decks.entries() {
            if key == "1" {
                // TODO Revisit if this works for an arbitrary deck.
                continue;
            }

            if let Some(name) = value["name"].as_str() {
                if name != "Default" {
                    self.set_name(name);
                }
            };

            if let Some(desc) = value["desc"].as_str() {
                self.set_property("desc", desc).unwrap();
            };
        }

        for (_key, value) in models.entries() {
            let model: Model = serde_json::from_str(&value.dump()).unwrap();
            let mut models = self_.models.borrow_mut();
            models.push(model);
        }

        let deck_conf = json::parse(&col.dconf).unwrap();
        if let Some(per_day) = deck_conf["1"]["new"]["perDay"].as_i32() {
            self.set_property("per-day", per_day).unwrap();
        };

        Ok(())
    }
}
