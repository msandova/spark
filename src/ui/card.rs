// SPDX-License-Identifier: GPL-3.0-or-later
use crate::obj;

use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::{prelude::*, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gitlab/gnome/msandova/Spark/ui/card.ui")]
    pub struct Card {
        #[template_child]
        stack: TemplateChild<gtk::Stack>,
        #[template_child]
        front_box: TemplateChild<gtk::Box>,
        #[template_child]
        back_box: TemplateChild<gtk::Box>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Card {
        const NAME: &'static str = "Card";
        type Type = super::Card;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Card {
        fn dispose(&self, _obj: &Self::Type) {
            self.stack.unparent();
        }
    }
    impl WidgetImpl for Card {}
}

glib::wrapper! {
    pub struct Card(ObjectSubclass<imp::Card>)
        @extends gtk::Widget;
}

impl Card {
    pub fn new(_card: &obj::Card) -> Self {
        glib::Object::new(&[]).unwrap()
    }
}
