// SPDX-License-Identifier: GPL-3.0-or-later
use crate::config;
use crate::obj::Deck;
use crate::ui::{DeckRow, View};

use gtk::subclass::prelude::*;
use gtk::{gio, glib};
use gtk::{prelude::*, CompositeTemplate};

mod imp {
    use super::*;

    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gitlab/gnome/msandova/Spark/ui/recent_decks.ui")]
    pub struct RecentDecks {
        pub deck_list: RefCell<Vec<String>>,

        #[template_child]
        pub decks: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub scrolled_window: TemplateChild<gtk::ScrolledWindow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for RecentDecks {
        const NAME: &'static str = "RecentDecks";
        type Type = super::RecentDecks;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for RecentDecks {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpec::new_boxed(
                    "deck-list",
                    "Decks",
                    "Decks",
                    Vec::<String>::static_type(),
                    glib::ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "deck-list" => self.deck_list.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "deck-list" => self.deck_list.replace(value.get().unwrap()),
                _ => unimplemented!(),
            };
        }
        fn constructed(&self, obj: &Self::Type) {
            let settings = gio::Settings::new(config::APP_ID);

            settings.bind("deck-list", obj, "deck-list").build();

            obj.connect_notify_local(Some("deck-list"), move |obj, _| {
                obj.populate();
            });

            self.decks
                .connect_row_activated(glib::clone!(@weak obj => move |_, row| {
                    let window = obj.root().unwrap().downcast::<crate::ui::Window>().unwrap();
                    let deck = row.downcast_ref::<DeckRow>().unwrap().deck();

                    window.set_view(View::Details(deck));
                }));

            self.parent_constructed(obj);
        }

        fn dispose(&self, _obj: &Self::Type) {
            self.scrolled_window.unparent();
        }
    }
    impl WidgetImpl for RecentDecks {}
}

glib::wrapper! {
    pub struct RecentDecks(ObjectSubclass<imp::RecentDecks>)
        @extends gtk::Widget;
}

impl Default for RecentDecks {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl RecentDecks {
    fn populate(&self) {
        let self_ = imp::RecentDecks::from_instance(self);

        let settings = gio::Settings::new(config::APP_ID);
        let decks: Vec<glib::Object> = settings
            .strv("deck-list")
            .iter()
            .map(|x| Deck::from_path(x.as_str()).upcast())
            .collect();

        let model = gio::ListStore::new(Deck::static_type());
        model.splice(0, 0, decks.as_slice());

        self_.decks.bind_model(Some(&model), move |obj| {
            let deck = obj.downcast_ref::<Deck>().unwrap();

            DeckRow::new(deck).upcast()
        });
    }
}
