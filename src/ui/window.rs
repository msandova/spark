// SPDX-License-Identifier: GPL-3.0-or-later
use crate::application::Application;
use crate::config::{APP_ID, PROFILE};
use crate::obj::{self, Deck};
use crate::ui::{self, CardPage, DeckDetails, RecentDecks};

use adw::prelude::*;
use gtk::{gio, glib, glib::clone, subclass::prelude::*};
use gtk_macros::spawn;

use gettextrs::gettext;

#[derive(Debug, Clone)]
pub enum View {
    RecentDecks,
    Details(Deck),
    Card(obj::Card),
}

impl Default for View {
    fn default() -> Self {
        Self::RecentDecks
    }
}

mod imp {
    use super::*;

    use adw::subclass::prelude::*;
    use gtk::CompositeTemplate;

    use std::cell::RefCell;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/org/gitlab/gnome/msandova/Spark/ui/window.ui")]
    pub struct Window {
        #[template_child]
        pub headerbar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub deck_details_bin: TemplateChild<adw::Bin>,
        #[template_child]
        pub leaflet: TemplateChild<adw::Leaflet>,
        #[template_child]
        pub card_page: TemplateChild<CardPage>,

        pub settings: gio::Settings,
        pub view: RefCell<View>,
    }

    impl Default for Window {
        fn default() -> Self {
            Self {
                headerbar: TemplateChild::default(),
                leaflet: TemplateChild::default(),
                deck_details_bin: TemplateChild::default(),
                card_page: TemplateChild::default(),

                view: RefCell::default(),
                settings: gio::Settings::new(APP_ID),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            RecentDecks::static_type();

            klass.install_action("add-deck", None, move |obj, _, _| {
                spawn!(clone!(@weak obj => async move {
                    obj.add_deck().await;
                }));
            });

            klass.install_action("about", None, move |obj, _, _| {
                obj.show_about_dialog();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load latest window state
            obj.load_window_size();
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        // Save window state on delete event
        fn close_request(&self, window: &Self::Type) -> gtk::Inhibit {
            if let Err(err) = window.save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request(window)
        }
    }

    impl ApplicationWindowImpl for Window {}
    impl AdwWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::Window, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl Window {
    pub fn new(app: &Application) -> Self {
        glib::Object::new(&[("application", app)]).expect("Failed to create Window")
    }

    async fn add_deck(&self) {
        let self_ = imp::Window::from_instance(self);

        let filter = gtk::FileFilter::new();
        filter.add_mime_type("application/x-apkg");
        filter.add_mime_type("application/x-anki");
        filter.add_mime_type("application/x-ankiaddon");
        filter.add_pattern("*.apkg");
        filter.set_name(Some(&gettext("Anki Deck")));

        let dialog = gtk::FileChooserNativeBuilder::new()
            .action(gtk::FileChooserAction::Open)
            .transient_for(self)
            .build();

        dialog.add_filter(&filter);

        let response = dialog.run_future().await;
        dialog.destroy();

        let mut strv = self_.settings.strv("deck-list");
        if response == gtk::ResponseType::Accept {
            let file = dialog.file().unwrap();

            let path: glib::GString = file.path().unwrap().to_str().unwrap().into();

            if !strv.contains(&path) {
                strv.push(path);
            }

            let strv_unowned: Vec<_> = strv.iter().map(glib::GString::as_str).collect();
            self_.settings.set_strv("deck-list", &strv_unowned).unwrap();
        };
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let self_ = imp::Window::from_instance(self);

        let (width, height) = self.default_size();

        self_.settings.set_int("window-width", width)?;
        self_.settings.set_int("window-height", height)?;

        self_
            .settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let self_ = imp::Window::from_instance(self);

        let width = self_.settings.int("window-width");
        let height = self_.settings.int("window-height");
        let is_maximized = self_.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    pub fn set_view(&self, view: View) {
        let self_ = imp::Window::from_instance(self);
        match view {
            View::RecentDecks => {
                self_.leaflet.set_visible_child_name("recent_decks");
            }
            View::Details(ref deck) => {
                let deck_details = DeckDetails::new(deck);
                self_.deck_details_bin.set_child(Some(&deck_details));
                self_.leaflet.set_visible_child_name("details");
            }
            View::Card(ref card) => {
                self_.card_page.set_child(&ui::Card::new(card));
                self_.leaflet.set_visible_child_name("card");
            }
        }

        self_.view.replace(view);
    }

    fn show_about_dialog(&self) {
        let dialog = gtk::AboutDialogBuilder::new()
            .logo_icon_name(APP_ID)
            .license_type(gtk::License::Gpl30)
            .website("https://gitlab.gnome.org/msandova/spark/")
            .version(crate::config::VERSION)
            .transient_for(self)
            .modal(true)
            .authors(vec!["Maximiliano Sandoval".into()])
            .artists(vec!["Maximiliano Sandoval".into()])
            .build();

        dialog.show();
    }
}
