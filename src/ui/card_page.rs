// SPDX-License-Identifier: GPL-3.0-or-later
use crate::ui::{Card, View, Window};

use adw::prelude::*;
use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gitlab/gnome/msandova/Spark/ui/card_page.ui")]
    pub struct CardPage {
        #[template_child]
        pub bin: TemplateChild<adw::Bin>,
        #[template_child]
        pub box_: TemplateChild<gtk::Box>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CardPage {
        const NAME: &'static str = "CardPage";
        type Type = super::CardPage;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();

            klass.install_action("go-back", None, move |obj, _, _| {
                let window = obj.root().unwrap().downcast::<Window>().unwrap();

                window.set_view(View::RecentDecks);
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for CardPage {
        fn dispose(&self, _obj: &Self::Type) {
            self.box_.unparent();
        }
    }
    impl WidgetImpl for CardPage {}
}

glib::wrapper! {
    pub struct CardPage(ObjectSubclass<imp::CardPage>)
        @extends gtk::Widget;
}

impl Default for CardPage {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl CardPage {
    pub fn set_child(&self, card: &Card) {
        let self_ = imp::CardPage::from_instance(self);

        self_.bin.set_child(Some(card));
    }
}
