// SPDX-License-Identifier: GPL-3.0-or-later
use crate::obj::Deck;

use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::{prelude::*, CompositeTemplate};

mod imp {
    use super::*;

    use adw::subclass::prelude::*;

    use gettextrs::gettext;
    use once_cell::sync::{Lazy, OnceCell};

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gitlab/gnome/msandova/Spark/ui/deck_row.ui")]
    pub struct DeckRow {
        pub deck: OnceCell<Deck>,

        #[template_child]
        pub due_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub new_label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DeckRow {
        const NAME: &'static str = "DeckRow";
        type Type = super::DeckRow;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("settings", None, move |obj, _, _| {
                obj.show_settings();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DeckRow {
        fn constructed(&self, obj: &Self::Type) {
            self.deck
                .get()
                .unwrap()
                .bind_property("name", obj, "title")
                .flags(glib::BindingFlags::SYNC_CREATE)
                .build();

            self.deck
                .get()
                .unwrap()
                .bind_property("per-day", &self.new_label.get(), "label")
                .transform_to(move |_, value| {
                    let per_day: i32 = value.get().unwrap();
                    let label = gettext!("New: {}", per_day);

                    Some(label.to_value())
                })
                .flags(glib::BindingFlags::SYNC_CREATE)
                .build();

            self.parent_constructed(obj);
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpec::new_object(
                    "deck",
                    "deck",
                    "deck",
                    Deck::static_type(),
                    glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "deck" => self.deck.get().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "deck" => self.deck.set(value.get().unwrap()).unwrap(),
                _ => unimplemented!(),
            };
        }
    }
    impl WidgetImpl for DeckRow {}
    impl ListBoxRowImpl for DeckRow {}
    impl ActionRowImpl for DeckRow {}
}

glib::wrapper! {
    pub struct DeckRow(ObjectSubclass<imp::DeckRow>)
        @extends gtk::Widget, gtk::ListBoxRow, adw::ActionRow;
}

impl DeckRow {
    pub fn new(deck: &Deck) -> Self {
        glib::Object::new(&[("deck", deck)]).unwrap()
    }

    fn show_settings(&self) {
        log::debug!("Not implemented");
    }

    pub fn deck(&self) -> Deck {
        let self_ = imp::DeckRow::from_instance(self);

        self_.deck.get().unwrap().clone() // TODO Maybe use a weak ref.
    }
}
