// SPDX-License-Identifier: GPL-3.0-or-later
use crate::obj::{self, Deck};
use crate::ui::{View, Window};

use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::{prelude::*, CompositeTemplate};

mod imp {
    use super::*;

    use once_cell::sync::{Lazy, OnceCell};

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gitlab/gnome/msandova/Spark/ui/deck_details.ui")]
    pub struct DeckDetails {
        pub deck: OnceCell<Deck>,

        #[template_child]
        pub name: TemplateChild<gtk::Label>,
        #[template_child]
        pub description: TemplateChild<gtk::Label>,
        #[template_child]
        pub box_: TemplateChild<gtk::Box>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DeckDetails {
        const NAME: &'static str = "DeckDetails";
        type Type = super::DeckDetails;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();

            klass.install_action("go-back", None, move |obj, _, _| {
                let window = obj.root().unwrap().downcast::<Window>().unwrap();

                window.set_view(View::RecentDecks);
            });

            klass.install_action("start", None, move |obj, _, _| {
                let window = obj.root().unwrap().downcast::<Window>().unwrap();

                let card = obj::Card::default();
                window.set_view(View::Card(card));
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DeckDetails {
        fn constructed(&self, obj: &Self::Type) {
            let deck = self.deck.get().unwrap();

            deck.bind_property("name", &self.name.get(), "label")
                .flags(glib::BindingFlags::SYNC_CREATE)
                .build();
            deck.bind_property("desc", &self.description.get(), "label")
                .flags(glib::BindingFlags::SYNC_CREATE)
                .build();

            self.parent_constructed(obj);
        }

        fn dispose(&self, _obj: &Self::Type) {
            self.box_.unparent();
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpec::new_object(
                    "deck",
                    "deck",
                    "deck",
                    Deck::static_type(),
                    glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "deck" => self.deck.get().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "deck" => self.deck.set(value.get().unwrap()).unwrap(),
                _ => unimplemented!(),
            };
        }
    }
    impl WidgetImpl for DeckDetails {}
}

glib::wrapper! {
    pub struct DeckDetails(ObjectSubclass<imp::DeckDetails>)
        @extends gtk::Widget;
}

impl DeckDetails {
    pub fn new(deck: &Deck) -> Self {
        glib::Object::new(&[("deck", deck)]).unwrap()
    }
}
