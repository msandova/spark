// SPDX-License-Identifier: GPL-3.0-or-later
use serde::Deserialize;

#[derive(Default, Debug, Clone, Deserialize)]
pub struct Field {
    font: String,
    media: Vec<String>,
    name: String,
    ord: u32,
    rtl: bool,
    size: u32,
    sticky: bool,
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct Template {
    afmt: String,
    bafmt: String,
    qfmt: String,
    bqfmt: String,
    did: Option<u64>,
    name: String,
    ord: u32,
}

#[derive(Default, Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Model {
    css: String,
    did: u64,
    flds: Vec<Field>,
    id: String,
    sortf: u32,
    latex_post: String,
    latex_pre: String,
    #[serde(rename = "mod")]
    mod_: u64,
    name: String,
    tags: Vec<String>,
    tmpls: Vec<Template>,
    #[serde(rename = "type")]
    type_: u32,
    usn: u32,
    vers: Vec<u32>,
}
