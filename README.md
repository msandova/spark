<!-- <a href="https://flathub.org/apps/details/org.gitlab.gnome.msandova.Spark"> -->
<!-- <img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" /> -->
<!-- </a> -->

# Spark

<img src="https://gitlab.gnome.org/msandova/spark/raw/master/data/icons/org.gitlab.gnome.msandova.Spark.svg" width="128" height="128" />

Learn with flashcards

## Screenshots

<div align="center">
![Main window](data/resources/screenshots/main_window.png "Main Window")
</div>

## Hack on Spark

To build the development version of Spark and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) when participating in project
spaces.

<!-- ## Translations -->

<!-- Helping to translate Spark or add support to a new language is very welcome. -->
<!-- You can find everything you need at: [l10n.gnome.org/module/spark/](https://l10n.gnome.org/module/spark/) -->
